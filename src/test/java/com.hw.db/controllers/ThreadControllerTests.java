package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.validation.constraints.Null;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ThreadControllerTests {
    private String slug;
    private String slug2;
    private int intId;
    private int intId2;
    private String stringId;
    private String stringId2;
    private Thread idThread;
    private Thread idThread2;
    private Thread slugThread;
    private ThreadController controller;
    private String name;
    private User user;
    private Post post;
    private Vote vote;

    @BeforeEach
    void setup() {
        slug = "slug";
        slug2 = "slug2";
        idThread = new Thread();
        idThread.setId(12345);
        idThread2 = new Thread();
        idThread2.setId(54321);
        slugThread = new Thread();
        slugThread.setSlug("slug");
        stringId = "12345";
        stringId2 = "54321";
        controller = new ThreadController();
        name = "tester";
        user = new User();
        user.setNickname(name);
        post = new Post();
        post.setAuthor(name);
        intId = 12345;
        intId2 = 54321;
        vote = new Vote(name, 123);
    }

    @Test
    @DisplayName("CheckIdOrSlug test: ID")
    void checkSlugAndIdInt() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(intId))
                    .thenReturn(idThread);
            assertEquals(idThread, controller.CheckIdOrSlug(stringId));
        }
    }

    @Test
    @DisplayName("CheckIdOrSlug test: Slug")
    void checkSlugAndIdString() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(slugThread);
            assertEquals(slugThread, controller.CheckIdOrSlug(slug));
        }
    }

    @Test
    @DisplayName("createPost test")
    void createPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> posts = new ArrayList<Post>();
            posts.add(post);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                    .thenReturn(slugThread);
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info(name))
                        .thenReturn(user);
                assertEquals(slugThread, ThreadDAO.getThreadBySlug(slug));
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts),
                        controller.createPost(slug, posts));
            }
        }
    }

    @Test
    @DisplayName("Posts test")
    void Posts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            final List<Post> posts = List.of(post);
            threadMock.when(() -> ThreadDAO.getThreadById(intId))
                    .thenReturn(idThread);
            threadMock.when(() -> ThreadDAO.getPosts(intId, 1, 10, "sort", false))
                    .thenReturn(posts);
            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(posts),
                    controller.Posts(stringId, 1, 10, "sort", false));
        }
    }

    @Test
    @DisplayName("change test")
    void change() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(intId)).thenReturn(idThread);
            threadMock.when(() -> ThreadDAO.change(idThread, idThread2)).thenAnswer((invocationOnMock) -> {
                threadMock.when(() -> ThreadDAO.getThreadById(intId)).thenReturn(idThread2);
                return null;
            });

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(idThread2),
                    controller.change(stringId, idThread2));
        }
    }

    @Test
    @DisplayName("info test")
    void info() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(slugThread);
            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(slugThread),
                    controller.info(slug));
        }
    }

    @Test
    @DisplayName("createVote test")
    void createVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                slugThread.setVotes(1);
                userMock.when(() -> UserDAO.Info(name)).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug))
                        .thenReturn(slugThread);
                threadMock.when(() -> ThreadDAO.change(vote, slugThread.getVotes()))
                        .thenReturn(slugThread.getVotes());
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(slugThread),
                        controller.createVote(slug, vote));
            }
        }
    }

}
